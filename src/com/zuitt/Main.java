package com.zuitt;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main (String[] args){
        Scanner in = new Scanner(System.in);
        try{
            System.out.println("Input an integer whose factorial will be computed ");

        } catch (InputMismatchException e) {
            System.out.println("Input is not a number");
        }
        int num = in.nextInt();
        int x = 1;
        int fact = 1;
        while (x <= num) {
            fact=fact*x;
            x++;
        }
        System.out.println("Factorial of "+num+" is: "+fact);

        System.out.println("Using For Loop with negative values");
        System.out.println("Input a negative integer whose factorial will be computed ");
        byte number = in.nextByte();
        int ex = 1;
        int factorial = 1;
        for (;ex<=number;ex++){
            factorial=factorial*ex;
        }
        System.out.println("Factorial of "+number+" is: "+fact);
    }
}
